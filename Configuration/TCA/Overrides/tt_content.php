<?php

// Contactform Plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'drk_contactform',
    'Contactform',
    'DRK Kontaktformular',
    'EXT:drk_contactform/Resources/Public/Icons/drk-logo-icon.svg',
    'DRK Kontaktformular'
);

$GLOBALS['TCA']['tt_content']['types'][$pluginSignature] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types'][$pluginSignature],
    [
        'showitem' => '
            --div--;General,
            --palette--;General;general,
            --palette--;Headers;headers'
    ]
);
