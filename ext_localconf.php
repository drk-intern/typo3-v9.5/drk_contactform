<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}
 \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'drk_contactform',
        'Contactform',
        [
            \DRK\DrkContactform\Controller\ContactFormController::class => 'showContactForm, send',
        ],
        // non-cacheable actions
        [
            \DRK\DrkContactform\Controller\ContactFormController::class  => 'send',
        ],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
