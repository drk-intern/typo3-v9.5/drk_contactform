/**
 * drk-contactform-js
 */

$(function () {
  'use strict';

  /**
   * main
   */

  toggleOthersubject();

});

/**
 * toggleOthersubject
 */
function toggleOthersubject() {
  if ($('select#subject').val() == '-1') {
    $('.tx-drk-contactform .item-othersubject').show();
    $('.tx-drk-contactform  input#othersubject').prop('disabled', false);
  } else {
    $('.tx-drk-contactform .item-othersubject').hide();
    $('.tx-drk-contactform input#othersubject').prop('disabled', true);

  }
}

