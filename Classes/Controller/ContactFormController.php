<?php

namespace DRK\DrkContactform\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Hans-Jürg Ungeheuer <ungeheuh@drk.de>, Deutsches Rotes Kreuz
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Controller\AbstractDrkController;
use DRK\DrkGeneral\Utilities\Utility;
use Psr\Http\Message\ResponseInterface;
use DRK\DrkContactform\Domain\Repository\ContactFormRepository;
use TYPO3\CMS\Core\Page\AssetCollector;

/**
 * ContactController
 */
class ContactFormController extends AbstractDrkController
{

    /**
     * contactFormRepository
     *
     * @var ContactFormRepository $contactFormRepository
     */
    protected $contactFormRepository;

    /**
     * @var array
     */
    protected $mandatoryFormFields = [
        'person' => ['vorname', 'name'],
    ];

    /**
     * Donation Purpose
     *
     * @var array
     */
    protected $subjectSelectionArray = [];

    /**
     * Recipient mail address
     *
     * @var string
     */
    protected $recipient = "";

    /**
     * @param AssetCollector $assetCollector
     */
    public function __construct(
        private readonly AssetCollector $assetCollector
    ) {
    }

    /**
     * Init
     *
     * @return void
     */
    protected function initializeAction(): void
    {
        parent::initializeAction();

        $this->assetCollector->addStyleSheet(
            'Contactform',
            'EXT:drk_contactform/Resources/Public/Css/style.css'
        );

        $this->assetCollector->addJavaScript(
            'Contactform',
            'EXT:drk_contactform/Resources/Public/Scripts/tx_drkcontactform.js',
            [],
            ['priority' => false]

        );

        if (!empty($this->settings['subjectSelection'])) {
            $this->subjectSelectionArray = explode(',', $this->settings['subjectSelection']) + array(-1 => "Sonstiges");
        } else {
            $this->subjectSelectionArray = array(-1 => "Sonstiges");
        }
    }

    /**
     * @param ContactFormRepository $conactFormRepository
     */
    public function injectContactRepository(ContactFormRepository $contactFormRepository)
    {
        $this->contactFormRepository = $contactFormRepository;
    }

    /**
     * action show
     *
     * @return void
     */
    public function showContactFormAction(): ResponseInterface
    {
        $this->view->assign('prefix_array', Utility::$prefixArray);
        $this->view->assign('title_array', Utility::$titleArray);
        $this->view->assign('subject_array', $this->subjectSelectionArray);
        $this->view->assign(
            'privacy_url',
            $this->validateUrl($this->settings['privacyUrl'] ?? null) ? $this->settings['privacyUrl'] : ''
        );

        if (!($this->recipient = filter_var($this->settings['recipient'] ?? '', FILTER_VALIDATE_EMAIL))) {
            $this->view->assign('error', true);
            $this->view->assign(
                'errorMessages',
                array('Fehler' => 'Bitte hinterlegen Sie eine korrekte Mailadresse des Empfängers!')
            );
        }

        return $this->htmlResponse();
    }

    /**
     * action send
     *
     * @return void|ResponseInterface
     */
    public function sendAction(): ResponseInterface
    {
        $aFormData = [];
        $this->view->assign('sending_ok', false);
        $this->view->assign('debug', $this->settings['debug']);
        $aArguments = $this->request->getArguments()['contactform'];

        $errors = false;
        if (!empty($aArguments)) {
            array_key_exists(
                'anrede',
                $aArguments
            ) ? $aFormData['anrede'] = intval($aArguments['anrede']) : $aFormData['anrede'] = 1;
            array_key_exists(
                'titel',
                $aArguments
            ) ? $aFormData['titel'] = intval($aArguments['titel']) : $aFormData['titel'] = 0;
            array_key_exists('name', $aArguments) ? $aFormData['name'] = $aArguments['name'] : $aFormData['name'] = "";
            array_key_exists(
                'vorname',
                $aArguments
            ) ? $aFormData['vorname'] = $aArguments['vorname'] : $aFormData['vorname'] = "";
            array_key_exists(
                'strasse',
                $aArguments
            ) ? $aFormData['strasse'] = $aArguments['strasse'] : $aFormData['strasse'] = "";
            array_key_exists('ort', $aArguments) ? $aFormData['ort'] = $aArguments['ort'] : $aFormData['ort'] = "";
            array_key_exists('plz', $aArguments) ? $aFormData['plz'] = $aArguments['plz'] : $aFormData['plz'] = "";
            array_key_exists(
                'telefon',
                $aArguments
            ) ? $aFormData['telefon'] = $aArguments['telefon'] : $aFormData['telefon'] = "";
            array_key_exists(
                'email',
                $aArguments
            ) ? $aFormData['email'] = $aArguments['email'] : $aFormData['email'] = "";
            array_key_exists(
                'message',
                $aArguments
            ) ? $aFormData['nachricht'] = $aArguments['message'] : $aFormData['nachricht'] = "";
            array_key_exists(
                'email_freigabe',
                $aArguments
            ) ? $aFormData['email_freigabe'] = intval($aArguments['email_freigabe']) : $aFormData['email_freigabe'] = 0;


            // get subject from form
            if (array_key_exists(
                'subject',
                $aArguments
            ) && is_numeric($aArguments['subject']) && $aArguments['subject'] != '-1') {
                $aFormData['betreff'] = array_key_exists(
                    $aArguments['subject'],
                    $this->subjectSelectionArray
                ) ? $this->subjectSelectionArray[$aArguments['subject']] : 'Sonstiges';
            } else {
                if (array_key_exists('othersubject', $aArguments) && $aArguments['othersubject'] != "") {
                    $aFormData['betreff'] = $aArguments['othersubject'];
                } else {
                    $aFormData['betreff'] = "Sonstiges";
                }
            }
        }

        //set recipient
        if (!($this->recipient = filter_var($this->settings['recipient'], FILTER_VALIDATE_EMAIL))) {
            $this->error = array('Fehler' => 'Bitte hinterlegen Sie eine korrekte Mailadresse des Empfängers!');
            $errors = true;
        } else {
            $aFormData['recipient'] = $this->recipient;
        }


        //check if we have a empty form
        $sCheck = $aArguments['name'] . $aArguments['vorname'] . $aArguments['email'];
        if (empty($sCheck)) {
            $this->error = array('Fehler' => 'Bitte kein leeres Formular absenden!');
            $errors = true;
        }

        $mandatoryFieldsFilled = $this->checkMandatoryFormFields($this->mandatoryFormFields['person'], $aFormData);

        //check honeypot field
        if (!$this->isHoneypotFilled($aArguments['birthname']))
        {
            $errors = true;
        }

        if (!$errors && $mandatoryFieldsFilled) {
            $request = [
                $this->settings['apiKey'],
                $aFormData
            ];

            // sending Data are successful

            if ($this->contactFormRepository->sendContactData($request)) {
                $this->view->assign('sending_ok', true);

                // if successPageId is set, the redirect to this page
                if ($this->settings['successPageId'] && !$this->settings['debug']) {
                    $uriBuilder = $this->uriBuilder;
                    $uriBuilder->reset();
                    $uriBuilder->setTargetPageUid($this->settings['successPageId']);
                    $uriBuilder->setCreateAbsoluteUri(true);
                    $uri = $uriBuilder->build();

                    return $this->responseFactory->createResponse(
                        303,
                        ''
                    )->withAddedHeader('Location', $uri);

                    exit;
                } // else go on an render the template
                else {
                    unset($request);

                    $aClientData = $aFormData;
                    $aClientData['anrede'] = array_key_exists(
                        $aFormData['anrede'],
                        Utility::$prefixArray
                    ) ? Utility::$prefixArray[$aFormData['anrede']] : "Herr";
                    $aClientData['titel'] = array_key_exists(
                        $aFormData['titel'],
                        Utility::$titleArray
                    ) ? Utility::$titleArray[$aFormData['titel']] : "";

                    $this->view->assign('aClientData', $aClientData);
                }
            }
        }

        // show debug
        if ($this->settings['debug']) {
            $this->view->assign('debug', $aArguments);
        }

        // if Json-Errors, then show it now
        if ($sError = $this->contactFormRepository->getErrors())
        {
            $this->error = array_merge($this->error, $sError);
            $errors = true;
        }

        if ($errors) {
            $this->view->assign('error', true);
            $this->view->assign('errorMessages', $this->error);
        } else {
            $this->view->assign('error', false);
        }
        return $this->htmlResponse();
    }
}
