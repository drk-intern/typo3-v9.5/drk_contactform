<?php

namespace DRK\DrkContactform\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Hans-Jürg Ungeheuer <ungeheuh@drk.de>, Deutsches Rotes Kreuz
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DRK\DrkGeneral\Domain\Repository\AbstractDrkRepository;
use DRK\DrkGeneral\Utilities\Utility;

/**
 * The repository for Contacts
 */
class ContactFormRepository extends AbstractDrkRepository
{

    /**
     *
     * sendContactData
     *
     * @param array $aSendObject
     * @return bool
     */
    public function sendContactData($aSendObject = [])
    {
        if (empty($aSendObject)) {
            $this->error = ['Error' => 'No object to send!'];
            return false;
        }

        try {
            $aResult = $this->executeJsonClientAction('sendContactData', $aSendObject);

            // Result is NULL, if no errors occur
            if (empty($aResult)) {
                return true;
            } else {
                return $aResult;
            }
        } catch (\Exception $exception) {
            $this->error = ['Fehler' => $exception->getMessage()];
            return false;
        }
    }
}
